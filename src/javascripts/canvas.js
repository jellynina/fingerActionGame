var can = document.getElementById('can'),
  ctx = can.getContext('2d'),
  keyUnitWidth,
  gutter = 10, // gutter space between the keybpard
  digit = 4, // How many digits in the game
  keyboardArray = []; // For drawing keyboard on canvas
// For touch event
var mouseIsDown = 0,
  day = new Date(),
  len = 0,
  touchAction = [], // hey KAITE!! here is the data you need!
  zones = [];
var canX = [],
  canY = [];

var colors = {
  canvasBg: 'rgba(42, 42, 46, 1)',
  keyboard: {
    default: 'rgba(196, 245, 241, 0.2)',
    active: 'rgb(224, 250, 255)'
  },
  circles: ['rgba(255, 20, 97, x)',
    'rgba(24, 255, 146, x)',
    'rgba(90, 135, 255, x)',
    'rgba(251, 243, 140, x)',
    'rgba(107, 8, 72, x)'
  ]
};

var randomNum = {
  pRadius: {
    max: 120,
    min: 60
  }
};

//For animation feedback
var numberOfParticules = 18;
var circles = [];

//=================End of Var declear =================

function init() {
  can.width = window.innerWidth;
  can.height = window.innerHeight;
  can.style.background = colors.canvasBg;
  keyUnitWidth = can.width / digit - gutter * 2;
  getZoneData();

}

// ensure canvas is always full size of browser window
function windowResize() {
  // If there is any resize happening // How about rotation?
  console.log('Flag: resizing');
  can.width = innerWidth;
  can.height = innerHeight;
  ctx.clearRect(0, 0, innerWidth, innerHeight);
  keyUnitWidth = can.width / digit - gutter * 2;
  getZoneData();
}



function touchEvent(event) {
  event.preventDefault();
  mouseIsDown = 1;
  len = event.targetTouches.length;
  var fingerActionData = {};
  fingerActionData.flge = true;
  fingerActionData.time = day.getTime();
  fingerActionData.fingers = [];
  // init the fingers
  for (var i = 0; i < digit; i++) fingerActionData.fingers.push(0);
  for (var i = 0; i < len; i++) {
    canX[i] = event.targetTouches[i].pageX - can.offsetLeft;
    canY[i] = event.targetTouches[i].pageY - can.offsetTop;
    drawGroup(canX[i], canY[i]);
    var fingerPosition = event.targetTouches[i].pageX - can.offsetLeft;
    if (fingerPosition < zones[0]) {
      fingerActionData.fingers[0] = 1;
      console.log("1:" + fingerPosition);
      keyboardArray[0].color = colors.keyboard.active;
      keyboardArray[0].update();
    } else if ((zones[0] - 1) < fingerPosition && fingerPosition < zones[1]) {
      fingerActionData.fingers[1] = 1;
      console.log("2: " + fingerPosition);
      keyboardArray[1].color = colors.keyboard.active;
      keyboardArray[1].update();
    } else if (zones[1] - 1 < fingerPosition && fingerPosition < zones[2]) {
      console.log("3:" + fingerPosition);
      fingerActionData.fingers[2] = 1;
      keyboardArray[2].color = colors.keyboard.active;
      keyboardArray[2].update();
    } else if (fingerPosition > zones[2]) {
      console.log("4:" + fingerPosition);
      fingerActionData.fingers[3] = 1;
      keyboardArray[3].color = colors.keyboard.active;
      keyboardArray[3].update();
    }
  }
  touchAction.push(fingerActionData);
}

function touchEnd(event) {
  //console.log('Touch End');
  mouseIsDown = 0;
  for (var i = 0; i < keyboardArray.length; i++) {
    keyboardArray[i].color = colors.keyboard.default;
  }

}


function getZoneData() {
  var unitWidth = can.width / digit;
  for (var i = 0; i < digit; i++) {
    var breaksPoints = unitWidth * (1 + i);
    zones.push(breaksPoints);

    var startX = gutter + i * unitWidth;
    keyboardArray.push(new KeyboardBlcok(startX, 10, colors.keyboard.default));
  }
  //console.log(`The breaks points ${zones}`);

}

// Canvas Object:
function KeyboardBlcok(x, y, color) {
  this.x = x;
  this.y = y;
  this.color = color;

  this.width = keyUnitWidth;
  this.height = window.innerHeight - 20;

  this.draw = function () {
    ctx.beginPath();
    ctx.rect(this.x, this.y, this.width, this.height);
    ctx.fillStyle = this.color;
    ctx.fill();
  }
  this.update = function () {
    this.draw();
  }

}

// Canvas Object:
function CirclePartical(x, y) {
  this.x = x;
  this.y = y;
  this.vx = (Math.random() - 0.5) * 5 + (Math.random() < 0.5 ? -2 : 2);
  this.vy = (Math.random() - 0.5) * 5 + (Math.random() < 0.5 ? -2 : 2);
  this.color = colors.circles[PAPU.random(0, colors.circles.length - 1)];
  this.radius = PAPU.random(randomNum.pRadius.min, randomNum.pRadius.max);
  this.alpha = 1;

  this.draw = function () {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    ctx.strokeStyle = this.color.replace('x', +this.alpha);
    ctx.lineWidth = 2;
    //ctx.globalAlpha = this.alpha;
    ctx.stroke();
  }
  this.update = function () {
    this.x += this.vx;
    this.y += this.vy;
    if (this.radius > 5) {
      this.radius -= PAPU.random(1, 5);
    }
    if (this.alpha > 0) {
      this.alpha -= 0.015;
    }
    this.draw();
  }
}


function drawGroup(x, y) {
  for (var i = 0; i < numberOfParticules; i++) {
    circles.push(new CirclePartical(x, y));
    circles[i].draw();
  }
}


function animate() {
  requestAnimationFrame(animate); // 怎麼寫成作用一段時間後消失？
  ctx.clearRect(0, 0, innerWidth, innerHeight);
  // Making the keyboard
  for (var i = 0; i < keyboardArray.length; i++) {
    keyboardArray[i].update();
  }
  if (circles.length > 1 && circles != undefined) {
    for (var i = 0; i < circles.length; i++) {
      // remove the circle if it is transparent or too small
      circles[i].update();
      if (circles[i].alpha < 0 || circles[i].radius < 5) {
        circles.splice(i, 1);
      }
    }
  }


}

function feedbackAnimate() {
  if (circles.length > 0) {
    requestAnimationFrame(feedbackAnimate);
    ctx.clearRect(0, 0, innerWidth, innerHeight);
    for (var i = 0; i < circles.length; i++) {
      // remove the circle if it is transparent or too small
      if (circles[i].alpha < 0 || circles[i].radius < 3) {
        circles.splice(i, 1);
      }
      circles[i].update();
    }
  }
}


// Random Machine
function PAPU() {
  // PAPU my babies instance
}
PAPU.random = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}



init();
window.addEventListener('resize', windowResize);

can.addEventListener("touchstart", touchEvent, false);
can.addEventListener("touchmove", touchEvent, false);
can.addEventListener("touchend", touchEnd, false);
animate();
